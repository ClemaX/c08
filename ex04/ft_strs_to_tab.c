/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_strs_to_tab.c                                 .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: chamada <marvin@le-101.fr>                 +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/08/15 02:40:50 by chamada      #+#   ##    ##    #+#       */
/*   Updated: 2019/08/15 19:15:13 by chamada     ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include <stdlib.h>
#include "ft_stock_str.h"

int					ft_strlen(char *str)
{
	const char *start = str;

	while (*str)
		str++;
	return (str - start);
}

char				*ft_strdup(char *src)
{
	char	*dup;
	char	*start;

	dup = malloc(sizeof(char) * (ft_strlen(src) + 1));
	if (!dup)
		return (NULL);
	start = dup;
	while (*src)
	{
		*dup = *src;
		src++;
		dup++;
	}
	*dup = '\0';
	return (start);
}

struct s_stock_str	*ft_strs_to_tab(int ac, char **av)
{
	t_stock_str *strs;
	int			len;
	int			i;

	if (!(strs = malloc(sizeof(t_stock_str) * (ac + 1))))
		return (NULL);
	i = 0;
	while (i < ac)
	{
		len = ft_strlen(av[i]);
		strs[i].size = len + 1;
		strs[i].str = av[i];
		if (!(strs[i].copy = ft_strdup(av[i])))
			return (NULL);
		i++;
	}
	strs[i].str = 0;
	return (strs);
}
