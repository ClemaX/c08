/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_show_tab.c                                    .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: chamada <marvin@le-101.fr>                 +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/08/15 03:05:26 by chamada      #+#   ##    ##    #+#       */
/*   Updated: 2019/08/15 18:52:15 by chamada     ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include <unistd.h>
#include "ft_stock_str.h"

void	ft_nextchar(unsigned int nb)
{
	char r;

	if (nb != 0)
	{
		r = (nb % 10) + '0';
		nb = nb / 10;
		ft_nextchar(nb);
		write(1, &r, 1);
	}
}

void	ft_putnbr(int nb)
{
	unsigned int u_nb;

	u_nb = nb;
	if (nb < 0)
	{
		u_nb *= -1;
		write(1, "-", 1);
	}
	else if (nb == 0)
		write(1, "0", 1);
	ft_nextchar(u_nb);
}

void	ft_show_tab(struct s_stock_str *par)
{
	int i;

	i = 0;
	while (par[i].str != 0)
	{
		write(1, par[i].str, par[i].size - 1);
		write(1, "\n", 1);
		ft_putnbr(par[i].size);
		write(1, "\n", 1);
		while (*par[i].copy)
			write(1, par[i].copy++, 1);
		write(1, "\n", 1);
		i++;
	}
}
